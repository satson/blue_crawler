
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:AppBundle" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="AppBundle.html">AppBundle</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:AppBundle_Controller" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="AppBundle/Controller.html">Controller</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:AppBundle_Controller_CrawlerController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="AppBundle/Controller/CrawlerController.html">CrawlerController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:AppBundle_Form" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="AppBundle/Form.html">Form</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:AppBundle_Form_CrawlerType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="AppBundle/Form/CrawlerType.html">CrawlerType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:AppBundle_Model" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="AppBundle/Model.html">Model</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:AppBundle_Model_CrawlerAbstract" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="AppBundle/Model/CrawlerAbstract.html">CrawlerAbstract</a>                    </div>                </li>                            <li data-name="class:AppBundle_Model_File" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="AppBundle/Model/File.html">File</a>                    </div>                </li>                            <li data-name="class:AppBundle_Model_ProfileCrawler" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="AppBundle/Model/ProfileCrawler.html">ProfileCrawler</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:AppBundle_AppBundle" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="AppBundle/AppBundle.html">AppBundle</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "AppBundle.html", "name": "AppBundle", "doc": "Namespace AppBundle"},{"type": "Namespace", "link": "AppBundle/Controller.html", "name": "AppBundle\\Controller", "doc": "Namespace AppBundle\\Controller"},{"type": "Namespace", "link": "AppBundle/Form.html", "name": "AppBundle\\Form", "doc": "Namespace AppBundle\\Form"},{"type": "Namespace", "link": "AppBundle/Model.html", "name": "AppBundle\\Model", "doc": "Namespace AppBundle\\Model"},
            
            {"type": "Class", "fromName": "AppBundle", "fromLink": "AppBundle.html", "link": "AppBundle/AppBundle.html", "name": "AppBundle\\AppBundle", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "AppBundle\\Controller", "fromLink": "AppBundle/Controller.html", "link": "AppBundle/Controller/CrawlerController.html", "name": "AppBundle\\Controller\\CrawlerController", "doc": "&quot;Class CrawlerController&quot;"},
                                                        {"type": "Method", "fromName": "AppBundle\\Controller\\CrawlerController", "fromLink": "AppBundle/Controller/CrawlerController.html", "link": "AppBundle/Controller/CrawlerController.html#method_indexAction", "name": "AppBundle\\Controller\\CrawlerController::indexAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Controller\\CrawlerController", "fromLink": "AppBundle/Controller/CrawlerController.html", "link": "AppBundle/Controller/CrawlerController.html#method_downloadFileAction", "name": "AppBundle\\Controller\\CrawlerController::downloadFileAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "AppBundle\\Form", "fromLink": "AppBundle/Form.html", "link": "AppBundle/Form/CrawlerType.html", "name": "AppBundle\\Form\\CrawlerType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "AppBundle\\Form\\CrawlerType", "fromLink": "AppBundle/Form/CrawlerType.html", "link": "AppBundle/Form/CrawlerType.html#method_buildForm", "name": "AppBundle\\Form\\CrawlerType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Form\\CrawlerType", "fromLink": "AppBundle/Form/CrawlerType.html", "link": "AppBundle/Form/CrawlerType.html#method_configureOptions", "name": "AppBundle\\Form\\CrawlerType::configureOptions", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Form\\CrawlerType", "fromLink": "AppBundle/Form/CrawlerType.html", "link": "AppBundle/Form/CrawlerType.html#method_getBlockPrefix", "name": "AppBundle\\Form\\CrawlerType::getBlockPrefix", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "AppBundle\\Model", "fromLink": "AppBundle/Model.html", "link": "AppBundle/Model/CrawlerAbstract.html", "name": "AppBundle\\Model\\CrawlerAbstract", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "AppBundle\\Model\\CrawlerAbstract", "fromLink": "AppBundle/Model/CrawlerAbstract.html", "link": "AppBundle/Model/CrawlerAbstract.html#method_search", "name": "AppBundle\\Model\\CrawlerAbstract::search", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\CrawlerAbstract", "fromLink": "AppBundle/Model/CrawlerAbstract.html", "link": "AppBundle/Model/CrawlerAbstract.html#method_doRequest", "name": "AppBundle\\Model\\CrawlerAbstract::doRequest", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\CrawlerAbstract", "fromLink": "AppBundle/Model/CrawlerAbstract.html", "link": "AppBundle/Model/CrawlerAbstract.html#method_clearData", "name": "AppBundle\\Model\\CrawlerAbstract::clearData", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\CrawlerAbstract", "fromLink": "AppBundle/Model/CrawlerAbstract.html", "link": "AppBundle/Model/CrawlerAbstract.html#method_processSearchResult", "name": "AppBundle\\Model\\CrawlerAbstract::processSearchResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "AppBundle\\Model", "fromLink": "AppBundle/Model.html", "link": "AppBundle/Model/File.html", "name": "AppBundle\\Model\\File", "doc": "&quot;Class File&quot;"},
                                                        {"type": "Method", "fromName": "AppBundle\\Model\\File", "fromLink": "AppBundle/Model/File.html", "link": "AppBundle/Model/File.html#method_saveDataToFile", "name": "AppBundle\\Model\\File::saveDataToFile", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\File", "fromLink": "AppBundle/Model/File.html", "link": "AppBundle/Model/File.html#method_downloadFileAction", "name": "AppBundle\\Model\\File::downloadFileAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "AppBundle\\Model", "fromLink": "AppBundle/Model.html", "link": "AppBundle/Model/ProfileCrawler.html", "name": "AppBundle\\Model\\ProfileCrawler", "doc": "&quot;Class ProfileCrawler&quot;"},
                                                        {"type": "Method", "fromName": "AppBundle\\Model\\ProfileCrawler", "fromLink": "AppBundle/Model/ProfileCrawler.html", "link": "AppBundle/Model/ProfileCrawler.html#method_searchProfile", "name": "AppBundle\\Model\\ProfileCrawler::searchProfile", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\ProfileCrawler", "fromLink": "AppBundle/Model/ProfileCrawler.html", "link": "AppBundle/Model/ProfileCrawler.html#method_processSearchResult", "name": "AppBundle\\Model\\ProfileCrawler::processSearchResult", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\ProfileCrawler", "fromLink": "AppBundle/Model/ProfileCrawler.html", "link": "AppBundle/Model/ProfileCrawler.html#method_geProfileDataArr", "name": "AppBundle\\Model\\ProfileCrawler::geProfileDataArr", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\ProfileCrawler", "fromLink": "AppBundle/Model/ProfileCrawler.html", "link": "AppBundle/Model/ProfileCrawler.html#method_getProfileByUrl", "name": "AppBundle\\Model\\ProfileCrawler::getProfileByUrl", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "AppBundle\\Model\\ProfileCrawler", "fromLink": "AppBundle/Model/ProfileCrawler.html", "link": "AppBundle/Model/ProfileCrawler.html#method_setProfileSelector", "name": "AppBundle\\Model\\ProfileCrawler::setProfileSelector", "doc": "&quot;&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


