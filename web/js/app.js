var app = angular.module('blueCrawler', []);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});

app.controller('searchController', ['$scope','doRequest','$sce', function($scope,doRequest,$sce) {

    var url = 'http://localhost:8000';


    $scope.IsVisible = false;


    $scope.resultLink = null;

    $scope.search = function(search) {
if($scope.searchInput.length != 0){
    $scope.resultHtml = null;
    document.getElementById('spinner').style.display = 'block';
    document.getElementById('ajax-background').style.display = 'block';

    doRequest.getData(url,$scope.searchInput).api.then(function (result) {
        $scope.resultLink = result.data.response;


        $scope.resultHtml = $sce.trustAsHtml(result.data.response);
        console.log(result);
    });
}





    };



}]);



app.service('doRequest', ['$http', function ($http) {
  //  delete $http.defaults.headers.common['X-Requested-With'];

    this.getData = function (link,keyword) {
        return {
            api: $http({
                method: 'POST',
                url: link,
                params: {'search':keyword},
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                document.getElementById('spinner').style.display = 'none';
                document.getElementById('ajax-background').style.display = 'none';
                return response;
            }, function errorCallback(response) {
                return 'ERROR';
            })
        };
    };
}]);