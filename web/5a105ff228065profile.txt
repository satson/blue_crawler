nick EnglishPro_RR
country United States
languages English
contact
B.A. Political Science, Pitzer College (Claremont Colleges), 2002
Experience: 14 years
**New students, please feel free to send me a message on LinkedIn.  I may have more availability than is shown on my BuddySchool calendar, thank you!.
de.linkedin.com/in/EnglishProRR  
 I have fourteen years' experience working in international education. I am a professional English language teacher, business English trainer, test prep tutor, and editor.  I also have five years' experience as the Academic Director of an international English language school located at Santa Barbara City College, in Santa Barbara,California, USA (Kaplan International English).  
Currently, I am living in Germany and am a corporate 'Business English Trainer' for Daimler/Mercedes Benz and Energie Baden-Württemberg AG (EnBW).  I am also a GMAT tutor for Manhattan Elite Prep. Furthermore, I am a consulting editor for a major German and worldwide publisher, Klett Sprachen Verlag. I am currently editing business English textbooks.  Finally, I also teach individual students online, whom are from all over the world.
 
. I teach all levels of English, beginning to proficiency (A1 to C2), all skills (reading, writing, listening, speaking), and grammar. I teach Business English, Technical English, English for Special Purposes (ESP), and English for Academic Purposes (EAP).  I also specialize in Test Prep tutoring for standardized tests such as TOEFL, IELTS, CAE, CPE, GRE, GMAT, SAT, TOEIC, BEC, BULATS, PTE, etc.
In addition, I am an experienced teacher trainer, including training and practicum arranging for student teachers earning their TESOL/TEFL teaching certificates at UCSB/SBCC respectively.  Including teacher training, I am a previous TESOL (Los Padres chapter, California, USA-CATESOL) board member (Membership Coordinator and annual Conference Committee organizer and Co-Chair; as well as having been a regular panel member and workshop presenter.
 de.linkedin.com/in/EnglishProRR