<?php

namespace AppBundle\Controller;

use AppBundle\Form\CrawlerType;
use AppBundle\Model\ProfileCrawler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Model\File;




/**
 * Class CrawlerController
 * @package AppBundle\Controller
 */
class CrawlerController extends Controller
{



    /**
     *
     * @param RequestInterface $request
     * @Route("/", name="crawler")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */


    public function indexAction(Request $request)
    {


        $crawlerSearchForm = new CrawlerType();

        $form = $this->createForm(CrawlerType::class, $crawlerSearchForm);

        //$form->handleRequest($request);


        if ($request->isXmlHttpRequest() && !empty($request->get('search'))) {


            //if($form->isSubmitted() && $form->isValid()){


            $profile = new ProfileCrawler();

            $profileData = $profile->searchProfile($request->get('search'), '//div[@class="profileMainTitle"]/a');

            if ($profileData) {

                $file = new File();
                $file->saveDataToFile($profile->geProfileDataArr());

                return new JsonResponse(['response' => '<a href="' . $request->getSchemeAndHttpHost() . '/download/' .$file::OUTPUT_FILE_NAME . '" class="btn btn-primary downloadLink" id="downloadLink">Download txt file</a>']);

            } else {

                return new JsonResponse(['response' => 'No result']);

            }

        }


        return $this->render(':crawler:index.html.twig', ['form' => $form->createView()]);
    }



    /**
     * @Route("/download/{fileName}", name="download_file")
     * @return AppBundle\Model\File
     *
     */
    public function downloadFileAction($fileName)
    {

        if(empty($fileName)){
            throw $this->createNotFoundException('File name can not be empty.');
        }

        $file = new File();
        return  $file->downloadFileAction($fileName);


    }

}
