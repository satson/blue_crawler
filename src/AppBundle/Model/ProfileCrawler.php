<?php
namespace AppBundle\Model;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Model\CrawlerAbstract;


/**
 * Class ProfileCrawler
 * @package AppBundle\Model
 */
class ProfileCrawler extends CrawlerAbstract {


    /**
     *@const url site to search
     */
    const SITE_URL = 'http://buddyschool.com/';

    /**
     * @var array
     */
    private $profileDataArr;

    /**
     *
     * @var array
     *
     */

    private $profileSelectorsToRead = [
        'general_information' =>[
            'selector' => '.generalData > tr',
            'function' => '',
            'name'     => 'Profile Information',
            'has_child'     => true

        ],
        'education_about_me' =>[
            'selector' => 'div.column > p',
            'function' => '',
            'name'     => 'Education & about Me',
            'has_child'     => true

        ]

    ];


    /**
     * @param string $searchWord
     * @param string $xPathString
     * @return bool|mixed
     */

    public function searchProfile($searchWord,$xPathString){

        $searchResult =  $this->search($searchWord,$xPathString);
        if($searchResult){

            return   $this->processSearchResult($searchResult);
        }

        return  false;

    }

    /**
     * @param Crawler $searchResult
     * @return bool
     */
    public function processSearchResult(Crawler $searchResult) {


      $lintToFirstProfile = $searchResult->extract(array('href'))[0];

        $profile =  $this->getProfileByUrl($lintToFirstProfile);


        foreach($this->profileSelectorsToRead as $key => $value){

            if($value['has_child']){

                foreach ($profile->filter($value['selector']) as $child) {

                    $this->profileDataArr[] = trim(preg_replace('/\s\s+/', ' ',  $child->textContent));
                }

            }else{
                $this->profileDataArr[] = $profile->filter($value['selector'])->{$value['function']}();

            }

        }

        if(!empty($this->profileDataArr)){
            $this->profileDataArr =  $this->clearData($this->profileDataArr);

            return true;
        }

        return false;

    }


    /**
     * @return array
     */

    public function geProfileDataArr(){

        return $this->profileDataArr;
    }

    /**
     * @param $url
     * @return Crawler
     */

    public function getProfileByUrl($url){

        return  $this->doRequest($url);
    }

    /**
     * @param array $selector
     */

    public function setProfileSelector(array $selector){

        $this->profileSelectors = $selector;
    }





}


?>