<?php
namespace AppBundle\Model;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;



/**
 * Class File
 * @package AppBundle\Model
 */
class File {

    const OUTPUT_FILE_NAME = 'artkul.txt';

    /**
     * @param $data input data to file
     */

    public function saveDataToFile($data)
    {
        $file = new Filesystem();
        $file->dumpFile(self::OUTPUT_FILE_NAME, implode("\n", $data));

    }


    /**
     * @Route("/download/{name}", name="download_file")
     *
     **/
    public function downloadFileAction($name)
    {
        $response = new BinaryFileResponse(__DIR__. '/../../../web/' . self::OUTPUT_FILE_NAME);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $name);
        return $response;
    }


}


?>