<?php namespace AppBundle\Model;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

abstract  class CrawlerAbstract {

    const SITE_URL = 'http://buddyschool.com/';

    /**
     * @param string $searchWord
     * @param string $xPathString
     * @return bool|Crawler
     */

    public function search($searchWord,$xPathString){

        $client = new Client();
        $crawler = $client->request('GET', self::SITE_URL);
        $form = $crawler->selectButton('searchZoneSubmit')->form();
        $crawler = $client->submit($form, array('keyword' => $searchWord));


        $isSearchResults = $crawler->filter('section')->eq(1)->text();

        if (strpos($isSearchResults, 'No search results ') === false) {

            return $crawler->filterXPath($xPathString);
        }else{

            return false;

        }


    }

    /**
     * @param string $url
     * @param string $method
     * @return Crawler
     */

    public function doRequest($url='',$method='GET'){
        $client = new Client();
        return $client->request($method, self::SITE_URL.$url);
    }

    /**
     * @param array $data
     * @return array
     */

    public function clearData(array $data){

        foreach($data as $key => $value){
            $data[$key] =  trim(preg_replace('/\s\s+/',' ', $value));
        }

        return array_filter($data);
    }


    /**
     * @param Crawler $searchResult
     * @return mixed
     */

    abstract protected function processSearchResult(Crawler $searchResult);
}